-- To mine the smooth stone converted by Botania Orechid
-- Place stone chest above turtle
-- Place ore chest below turtle
local STONE_SLOT = 1
local ORE_SLOT = 2
local BUFFER_SLOT = 15
local SENTINEL_SLOT = 16



local function wait()
    os.sleep(0.2)
end


local function waitForSuckUp(n)
    turtle.select(STONE_SLOT)

    local numSucked = turtle.getItemCount()
    while not turtle.suckUp(n) do
        print("Could not suck up")
        os.sleep(5)
    end

    numSucked = turtle.getItemCount() - numSucked
    if numSucked < n then
        waitForSuckUp(n - numSucked)
    end
end


local function waitForPlace()
    local slot = turtle.getSelectedSlot()

    while not turtle.place() do
        if turtle.getItemCount() == 0 then
            print("No placeable block in slot " .. slot .. "!")
        else
            print("Could not place")
            if turtle.detect() then
                print("Attempting to clear obstruction...")
            end
            turtle.select(BUFFER_SLOT)
            turtle.dig()
            turtle.select(slot)
        end
        os.sleep(5)
    end
end


local function placeStone()
    local itemCount = turtle.getItemCount(STONE_SLOT)
    print(itemCount .. " items in STONE_SLOT")

    local suckCount = math.max(0, 2 - itemCount)
    print("SuckUp: " .. suckCount .. " stone!")
    waitForSuckUp(suckCount)
    wait()
    waitForPlace()
end


local function waitForConversion()
    turtle.select(STONE_SLOT)
    local success, block = turtle.inspect()
    while block.name == "minecraft:stone" do
        print("Waiting for conversion...")
        success, block = turtle.inspect()
        os.sleep(5)
    end
    print(block.name .. " detected!")
    wait()
end


local function waitForDrop()
    if turtle.getItemCount() > 0 then
        while not turtle.dropDown() do
            print("Could not drop")
            os.sleep(5)
        end
    end
end


local function dropOre()
    turtle.dig()
    for i = ORE_SLOT, SENTINEL_SLOT - 1, 1 do
        turtle.select(i)
        os.sleep(0.05)
        waitForDrop()
    end
end


local function isValidSentinel()
    turtle.select(SENTINEL_SLOT)
    return turtle.getItemCount() == 0
end


while isValidSentinel() do
    placeStone()
    waitForConversion()
    dropOre()
end


