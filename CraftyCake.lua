-- Face turtle NORTH
-- Ender chests are to be placed as follows:
-- N: Milk
-- E: Sugar
-- S: Eggs
-- W: Wheat

local function wait()
    os.sleep(0.2)
end


local function waitForSuck(slot)
    slot = slot or 1
    turtle.select(slot)
    while not turtle.suck(1) do
        print("Could not suck")
        os.sleep(5)
    end
end


local function waitForSuckDown(slot)
    slot = slot or 1
    turtle.select(slot)
    while not turtle.suckDown(1) do
        print("Could not suckDown")
        os.sleep(5)
    end
end


local function suckIngredients()
    -- Row 1
    -- | Milk | Milk | Milk |
    for i = 1, 3, 1 do
        waitForSuck(i)
        wait()
    end

    turtle.turnRight()

    -- Row 2
    -- | Sugar | Egg | Sugar |
    for i = 5, 7, 1 do
        if i % 2 == 1 then
            waitForSuck(i)
        else
            waitForSuckDown(i)
        end
        wait()
    end

    -- Row 3
    -- | Wheat | Wheat | Wheat |
    turtle.turnRight()

    for i = 9, 11, 1 do
        waitForSuck(i)
        wait()
    end
end


local function cakeUp()
	turtle.craft()
    while(turtle.detectUp()) do
        print("Obstruction above")
		os.sleep(4)
	end

	turtle.placeUp()
end


local function waitForDrop(slot)
    slot = slot or 1
    turtle.select(slot)
    while not turtle.drop() do
        print("Could not drop")
        os.sleep(5)
    end
end


local function returnBuckets()
    turtle.turnRight()
    for i = 3, 1, -1 do
        waitForDrop(i)
    end
    turtle.turnRight()
end


local function generateCake()
    suckIngredients()
    cakeUp()
    returnBuckets()
end


local function isValidSentinel()
    turtle.select(1)
    return turtle.getItemCount() == 0
end


print("It's a piece of cake to bake a pretty cake!")

while(isValidSentinel()) do
	generateCake()
end
